#include <LGPS.h>
#include <LGSM.h> 
#include <LBattery.h>
#define LED 2 //connect LED to digital pin2
#define LED2 3


char buff[256];

int temps;  
String message; 
gpsSentenceInfoStruct info;

//GPS code 
static unsigned char getComma(unsigned char num,const char *str)
{
  unsigned char i,j = 0;
  int len=strlen(str);
  for(i = 0;i < len;i ++)
  {
     if(str[i] == ',')
      j++;
     if(j == num)
      return i + 1; 
  }
  return 0; 
}

static double getDoubleNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atof(buf);
  return rev; 
}

static double getIntNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atoi(buf);
  return rev; 
}

void parseGPGGA(const char* GPGGAstr)
{

  double latitude;
  double longitude;
  int tmp, hour, minute, second, num ;
  if(GPGGAstr[0] == '$')
  {
    tmp = getComma(1, GPGGAstr);
    hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
    minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
    second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
    
    sprintf(buff, "UTC timer %2d-%2d-%2d", hour, minute, second);
    Serial.println(buff);
    
    tmp = getComma(2, GPGGAstr);
    latitude = getDoubleNumber(&GPGGAstr[tmp]);
    tmp = getComma(4, GPGGAstr);
    longitude = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff, "latitude = %10.4f, longitude = %10.4f", latitude, longitude);
    envoieSMS(buff);// Call the SMS function which allows to send the latitude and the longitude
    Serial.println(buff); 
    
    tmp = getComma(7, GPGGAstr);
    num = getIntNumber(&GPGGAstr[tmp]);    
    sprintf(buff, "satellites number = %d", num);
    Serial.println(buff); 
  }
  else
  {
    Serial.println("Not get data"); 
  }
}

void setup() {
  // initialize the digital pin2 as an output.
  pinMode(LED, OUTPUT); 
  pinMode(LED2, OUTPUT);    

  // put your setup code here, to run once:
  Serial.begin(115200);
  LGPS.powerOn();
  Serial.println("LGPS Power on, and waiting ..."); 
  delay(3000);
  while (!LSMS.ready())                 // Wait for the sim to initialize
  {
    delay(1000);                        // Wait for a second and then try again
  }
  
  Serial.println("La SIM est prête a envoyer le SMS");    // When SIM is started, print "Sim initialized" in the serial port

  LSMS.beginSMS("0692130691");          // Saves the number where user wants to send SMS. To be changed before uploading sketch

}
void batterie() {
  sprintf(buff,"battery level = %d", LBattery.level() );//We post the percentage of the battery via the serial port
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  
  if ( LBattery.level() > 20){// When the battery level is HIGH first led flash
  digitalWrite(LED, HIGH);// set the LED on
  delay(1000);
  
  digitalWrite(LED, LOW);// set the LED off
  delay(100);
  
  }
  else if ( LBattery.level()< 20) {// When the battery level is LOW second led flash
     digitalWrite(LED2, HIGH);// set the LED on
  delay(100);
  
  digitalWrite(LED2, LOW);// set the LED off
  delay(50);
    
  }
  delay(1000);
}

void envoieSMS(String m){
  
  message="Position de la Cle: "+m;// Variable who countain SMS

  LSMS.print(message);                  // Prepare message variable to be sent by LSMS

  
 

         
    if (LSMS.endSMS())                  // If so, send the SMS
    {
      Serial.println("Position envoyée"); // Print "SMS sent" in serial port if sending is successful
 
      
    }
    else
    {
      Serial.println("SMS is not sent");// Else print "SMS is not sent"
    }
  

  
  }

void loop() {
  // put your main code here, to run repeatedly:
 temps=6000;
 for (int i = 0; i<(temps/1000) ; i++){
    batterie();
    delay(1000);
  }
 

  Serial.println("LGPS loop"); 
  LGPS.getData(&info);
  Serial.println((char*)info.GPGGA);
  parseGPGGA((const char*)info.GPGGA);
 
  
  }

